import * as types from './../actions/types'

const orderReducer = (state = [], action) => {
    switch (action.type) {
        case types.CREATE_STORE:
            return {...state, loading: true};
        case types.CREATE_STORE_SUCCESS:
            return {...state, loading: false, data: action.data};
        case types.CREATE_STORE_ERROR:
            return {...state, loading: false, err: action.error};
        case types.GET_ALL_STORES:
            return {...state, storeListLoading: true};
        case types.GET_ALL_STORES_SUCCESS:
            return {...state, storeListLoading: false, allStores: action.data};
        case types.GET_ALL_STORES_ERROR:
            return {...state, storeListLoading: false, err: action.error};
        case types.CREATE_ORDER:
            return {...state, orderCreateLoading: true};
        case types.CREATE_ORDER_SUCCESS:
            return {...state, orderCreateLoading: false, newOrder: action.data};
        case types.CREATE_ORDER_ERROR:
            return {...state, orderCreateLoading: false, err: action.error};
        case types.GET_ALL_ORDERS:
            return {...state, orderListLoading: true};
        case types.GET_ALL_ORDERS_SUCCESS:
            return {...state, orderListLoading: false, allOrders: action.data};
        case types.GET_ALL_ORDERS_ERROR:
            return {...state, orderListLoading: false, err: action.error};
        case types.ADD_STORE:
            state.allStores.push(action.data);
            return {...state, allStores: state.allStores};
        case types.ADD_ORDER:
            state.allOrders.push(action.data);
            return {...state, allOrders: state.allOrders}
        default:
            return state;
    }
} 
export default orderReducer;