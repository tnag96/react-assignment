import React from 'react';
import logo from './logo.svg';
import './App.css';
import * as firebase from 'firebase';
import { FirebaseConfig } from './config/appConfig';
import 'firebase/database';
import CreateStore from './components/createStore';
import { Grid, AppBar, Toolbar, IconButton, Typography, Button, Drawer, makeStyles, useTheme, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import CreateOrder from './components/createOrder';
import MapWrapper from './components/map';
import { Provider } from 'react-redux'
import store from './config/store'
import MenuIcon from '@material-ui/icons/Menu';
import ViewStore from './components/viewStore';
import ViewOrder from './components/viewOrders'

const leftMenuWidth = 250;

function App() {
  if (!firebase.apps.length) {
    firebase.initializeApp(FirebaseConfig);
  }
  const menuItems = ['Home', 'Create Store', 'View Stores', 'Create Order', 'View Orders'];
  const [state, setState] = React.useState({
    left: false,
    right: false,
    mainContent: menuItems[0],
    rightMenuContent: null
  });
  const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      width: '100%',
      height: '100%',
      flexDirection: 'column'
    },
    container: {
      // width: 400,
      padding: 20,
      maxWidth: 'calc(100% - 40px)',
      minWidth: 'none'
    },
    rightDrawerPaper: {
      width: 'fit-content',
      minWidth: 'none'
    },
    appBar: {
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      width: `calc(100% - ${leftMenuWidth}px)`,
      marginLeft: leftMenuWidth,
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    hide: {
      display: 'none',
    },
    drawer: {
      width: leftMenuWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: leftMenuWidth,
    },
    drawerHeader: {
      display: 'flex',
      alignItems: 'center',
      padding: theme.spacing(0, 1),
      justifyContent: 'flex-end',
    },
    content: {
      flexGrow: 1,
      padding: 0,
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      marginLeft: -leftMenuWidth,
      paddingLeft: leftMenuWidth
    },
    contentShift: {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0
    },
  }));
  const classes = useStyles();
  const toggleRightMenu = (menu) => {
    if (menu === menuItems[1] || menu === menuItems[3]) {
      setState({...state, right: true, rightMenuContent: menu})
    } else {
      setState({...state, mainContent: menu})
    }
  }

  return (
    <Provider store={store}>
      <div className={classes.root}>
        <AppBar position="static" className={[classes.appBar, state.left ? classes.appBarShift : ''].join(' ')}>
          <Toolbar>
            <IconButton edge="start" color="inherit" aria-label="menu" onClick={() => setState({...state, left: !state.left })}>
              <MenuIcon />
            </IconButton>
            <Typography variant="h6">
              Sample Project
          </Typography>
            {/* <Button color="inherit">Login</Button> */}
          </Toolbar>
        </AppBar>
        <Drawer anchor='right' open={state.right} classes={{paper: classes.rightDrawerPaper}} onClose={() => setState({...state, right: false })}>
          <div className={classes.container}>
            { state.rightMenuContent === menuItems[1] ? 
              <>
                <h3>Create Store</h3>
                <CreateStore closeMenu={() => setState({...state, right: false})} />
              </> : <>
                <h3>Create Order</h3>
                <CreateOrder closeMenu={() => setState({...state, right: false})} />
              </>
            }
          </div>
        </Drawer>
        <Drawer anchor='left' variant='persistent' classes={{ paper: classes.drawerPaper }} className={classes.drawer} open={state.left} onClose={() => setState({...state, left: false })}>
          <List>
            {menuItems.map((text, index) => (
              <ListItem button key={text} onClick={() => toggleRightMenu(text)}>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>
        </Drawer>
        <main className={[state.mainContent === menuItems[0] ? 'map-container' : '' , classes.content, state.left ? classes.contentShift : ''].join(' ')}>
          {(() => {
            switch(state.mainContent) {
              case menuItems[0]: return <MapWrapper/>;
              case menuItems[2]: return <ViewStore />;
              case menuItems[4]: return <ViewOrder />;
              default: return <MapWrapper/>
            }
          })()}
        </main>
      </div>
    </Provider>
  );
}

export default App;
