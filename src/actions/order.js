import * as types from './types';
import { createStoreApi, getAllStoresApi, createOrderApi, getAllOrdersApi } from './../services/apiService';

const createStoreSuccess = (data) => {
    return { type: types.CREATE_STORE_SUCCESS, data}
};

const createStoreError = (error) => {
    return { type: types.CREATE_STORE_ERROR, error}
};


export const createStore = (data, callback) => (dispatch) => {
    dispatch({type: types.CREATE_STORE});
    createStoreApi(data).then(res => {
        callback && callback(res);
        dispatch(createStoreSuccess(res));
    }).catch(err => {
        dispatch(createStoreError(err));
    })
}


export const getAllStores = (callback) => (dispatch) => {
    dispatch({type: types.GET_ALL_STORES});
    getAllStoresApi().then(res => {
        callback && callback(res);
        dispatch({type: types.GET_ALL_STORES_SUCCESS, data: res});
    }).catch(err => {
        dispatch({type: types.GET_ALL_STORES_ERROR, error: err});
    })
}

export const addStore = (data) => (dispatch) => {
    dispatch({type: types.ADD_STORE, data });
}

export const addOrder = (data) => (dispatch) => {
    dispatch({type: types.ADD_ORDER, data });
}

export const getAllOrders = (callback) => (dispatch) => {
    dispatch({type: types.GET_ALL_ORDERS});
    getAllOrdersApi().then(res => {
        callback && callback(res);
        dispatch({type: types.GET_ALL_ORDERS_SUCCESS, data: res});
    }).catch(err => {
        dispatch({type: types.GET_ALL_ORDERS_ERROR, error: err});
    })
}



export const createOrder = (data, callback) => (dispatch) => {
    dispatch({type: types.CREATE_ORDER});
    createOrderApi(data).then(res => {
        callback && callback(res);
        dispatch({type: types.CREATE_ORDER_SUCCESS, data: res});
    }).catch(err => {
        dispatch({ type: types.CREATE_ORDER_ERROR, error: err});
    })
}