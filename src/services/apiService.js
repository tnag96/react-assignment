import * as firebase from 'firebase';
import 'firebase/database';

export const createStoreApi = (data) => {
    return new Promise((resolve, reject) => {
        const ref = firebase.database().ref('Stores');
        ref.push(data).then(res => {
            resolve({...data, id: res.key})
        }).catch(err => reject(err))
    })
}

export const getAllStoresApi = () => {
    return new Promise((resolve, reject) => {
        const ref = firebase.database().ref('Stores');
        ref.once('value').then(snap => {
            let res = snap.val()
            res = Object.keys(res).map(e => {
                return {...res[e], id: e};
            });
            resolve(res)
        }).catch(err => reject(err))
    })
}

export const createOrderApi = (data) => {
    return new Promise((resolve, reject) => {
        const ref = firebase.database().ref('Orders');
        ref.push(data).then(res => {
            resolve({...data, id: res.key})
        }).catch(err => reject(err))
    })
}

export const getAllOrdersApi = () => {
    return new Promise((resolve, reject) => {
        const ref = firebase.database().ref('Orders');
        ref.once('value').then(snap => {
            let res = snap.val()
            res = Object.keys(res).map(e => {
                return {...res[e], id: e};
            });
            resolve(res)
        }).catch(err => reject(err))
    })
}