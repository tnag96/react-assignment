import React, { Component } from 'react';
import { Button, TextField } from '@material-ui/core';
import { connect } from 'react-redux';
import { createStore, addStore } from './../actions/order';


export class CreateStore extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dirty: {},
            data: {
                name: '',
                lat: 0,
                long: 0,
                image: ''
            },
        }
        this.form = React.createRef();
    }

    componentWillReceiveProps(props) {
        console.log(props)
    }


    handleFormChange(event) {
        event.persist();
        console.log(event.target)
        this.setState({
            data: {...this.state.data,  
                    [event.target.name]: event.target.name === 'image' ? event.target.files[0] : event.target.value
                },
            dirty: {...this.state.dirty, [event.target.name]: true},
        })
    }

    isValidForm() {
        for(let i in this.state.data) {
            if (!this.state.data[i]) {
                return false;
            }
        }
        return true;
    }

    toBase64(file) { 
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    async submitForm() {
        const base64 = await this.toBase64(this.state.data.image);
        const data = {...this.state.data, image: base64};
        console.log(data);
        this.props.dispatch(createStore(data, (res) => {
            this.form.current.reset();
            this.setState({
                dirty: {},
                data: {
                    name: '',
                    lat: 0,
                    long: 0,
                    image: ''
                },
            })
            this.props.closeMenu();
            this.props.dispatch(addStore(data))
        }))
    }

    render() {
        const {data, dirty} = this.state;
        return (
            <form ref={this.form} onChange={(event) => this.handleFormChange(event)} style={styles.form} noValidate autoComplete="off">
                <TextField 
                    style={styles.inputs} 
                    label="Store Name" 
                    variant='outlined'
                    required
                    name='name'
                    // value={data.name}
                    error={dirty.name && !data.name}
                    helperText={dirty.name && !data.name ? 'Name is required' : ''}
                />
                <TextField 
                    style={styles.inputs} 
                    label="Store Image" 
                    InputLabelProps={{shrink: true}} 
                    variant='outlined' 
                    type='file'
                    // value={data.image}
                    required
                    name='image'
                    inputProps={{accept:'image/*'}}
                    error={dirty.image && !data.image}
                    helperText={dirty.image && !data.image ? 'Image is required' : ''} 
                />
                <TextField 
                    style={styles.inputs} 
                    label="Latitude" 
                    type='number' 
                    variant="outlined" 
                    required
                    name='lat'
                    // value={data.lat}
                    error={dirty.lat && !data.lat}
                    helperText={dirty.lat && !data.lat ? 'Latitude is required' : ''}
                />
                <TextField 
                    style={styles.inputs} 
                    label="Longitude" 
                    type='number' 
                    variant="outlined" 
                    required
                    name='long'
                    // value={data.long}
                    error={dirty.long && !data.long}
                    helperText={dirty.long && !data.long ? 'Longitude is required' : ''}
                />
                <Button 
                    style={styles.btn} 
                    color='primary' 
                    variant='contained'
                    disabled={this.props.order.loading || !this.isValidForm()}
                    onClick={() => this.submitForm()} 
                >{this.props.order.loading ? 'Creating...' : 'Create'}</Button>
            </form>
        )
    }
}


const styles = {
    form: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    inputs: {
        width: '100%',
        marginBottom: '20px'
    },
    btn:{
        width: '200px',
        textAlign: 'center',
        padding: '10px',
        marginBottom: '20px'
    }
}

const mapStateToProps = state => state;
export default connect(mapStateToProps)(CreateStore);