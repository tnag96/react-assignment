import React, { Component } from "react";
import { getAllStores } from "../actions/order";
import { TableContainer, Table, TableHead, TableRow, TableCell, Paper, TableBody } from "@material-ui/core";
import { connect } from "react-redux";



export class ViewStore extends Component {

    constructor(props) {
        super(props);
        this.state = {
            storeList: []
        }
    }

    componentDidMount() {
        this.props.dispatch(getAllStores((res) => {
            this.setState({ storeList: res });
        }))
    }

    render() {
        return (
            <div style={styles.container}>
                <TableContainer component={Paper}>
                    <Table size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Index</TableCell>
                                {/* <TableCell>Id</TableCell> */}
                                <TableCell>Name</TableCell>
                                <TableCell align="right">Latitude</TableCell>
                                <TableCell align="right">Longitude</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.storeList.map((row, i) => (
                                <TableRow key={row.name}>
                                    <TableCell>{i + 1}</TableCell>
                                    {/* <TableCell>{row.id}</TableCell> */}
                                    <TableCell style={styles.imgContainer}>
                                        <img style={styles.img} src={row.image} />
                                        <b>{row.name}</b>
                                    </TableCell>
                                    <TableCell align="right">{row.lat}</TableCell>
                                    <TableCell align="right">{row.long}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        )
    }
}

const styles = {
    container: {
        padding: 20
    },
    img: {
        height: 45,
        width: 60,
        objectFit: 'cover',
        marginRight: 20
    },
    imgContainer: {
        display: 'flex',
        alignItems: 'center'
    }
}

const mapStateToProps = state => state;
export default connect(mapStateToProps)(ViewStore);