import React, { Component } from 'react';
import { Map, GoogleApiWrapper } from 'google-maps-react';
import { Google_Api_Key } from './../config/appConfig';

export class MapWrapper extends Component {
    

    map;
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <Map
                google={this.props.google}
                initialCenter={{
                lat: 40.854885,
                lng: -88.081807
              }}
              style={styles.container}
              zoom={15}
            />
        )
    }
}

const styles = {
    container: {
        position: 'relative',
        width: '100%',
        height: '100%',
    }
}

export default GoogleApiWrapper({apiKey: Google_Api_Key})(MapWrapper);