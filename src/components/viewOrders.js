import React, { Component } from "react";
import { getAllOrders } from "../actions/order";
import { TableContainer, Table, TableHead, TableRow, TableCell, Paper, TableBody } from "@material-ui/core";
import { connect } from "react-redux";



export class ViewStore extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orderList: []
        }
    }

    componentDidMount() {
        this.props.dispatch(getAllOrders((res) => {
            this.setState({ orderList: res });
        }))
    }

    render() {
        return (
            <div style={styles.container}>
                <TableContainer component={Paper}>
                    <Table size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Index</TableCell>
                                {/* <TableCell>Id</TableCell> */}
                                <TableCell>Store Id</TableCell>
                                <TableCell align="right">Order Number</TableCell>
                                <TableCell align="right">Order Amount</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.orderList.map((row, i) => (
                                <TableRow key={row.id}>
                                    <TableCell>{i + 1}</TableCell>
                                    {/* <TableCell>{row.id}</TableCell> */}
                                    <TableCell>
                                        <b>{row.store_id}</b>
                                    </TableCell>
                                    <TableCell align="right">{row.orderNo}</TableCell>
                                    <TableCell align="right">{row.orderAmt}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        )
    }
}

const styles = {
    container: {
        padding: 20
    }
}

const mapStateToProps = state => state;
export default connect(mapStateToProps)(ViewStore);