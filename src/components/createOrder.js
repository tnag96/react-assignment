import React, { Component } from 'react';
import { Button, TextField, Select, MenuItem, InputLabel, FormControl } from '@material-ui/core';
import { connect } from 'react-redux';
import { getAllStores, createOrder, addOrder } from '../actions/order';


export class CreateOrder extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dirty: {},
            data: {
                orderNo: '',
                orderAmt: 0,
                store_id: ''
            },
            storeList: []
        }
        this.form = React.createRef();
    }

    componentDidMount() {
        this.props.dispatch(getAllStores())
    }

    componentWillReceiveProps(props) {
        if (this.props.order !== props.order && props.order.allStores) {
            this.setState({
                storeList: props.order.allStores,
                data: {...this.state.data, store_id: props.order.allStores[0].id}
            })
        }
    }

    handleFormChange(event) {
        event.persist();
        console.log(event)
        this.setState({
            data: {...this.state.data,  [event.target.name]: event.target.value},
            dirty: {...this.state.dirty, [event.target.name]: true}
        })
    }

    submitForm() {
        console.log(this.state);
        this.props.dispatch(createOrder(this.state.data, (res) => {
            this.form.current.reset();
            this.setState({
                data: {
                    orderNo: '',
                    orderAmt: 0,
                    store_id: this.state.storeList[0].id
                },
                dirty: {}
            })
            this.props.closeMenu();
            this.props.dispatch(addOrder(res))
        }))
    }

    render() {
        const {data, dirty, storeList} = this.state;
        const { storeListLoading, orderCreateLoading } = this.props.order
        return (
            <form ref={this.form} onChange={(event) => this.handleFormChange(event)} style={styles.form} noValidate autoComplete="off">
                <FormControl  style={styles.inputs}>
                    <InputLabel id="demo-simple-select-label">Select Store</InputLabel>
                    <Select
                        label="demo-simple-select-label"
                        onChange={(event) => this.handleFormChange(event)}
                        name='store_id'
                        error={dirty.store_id && !data.store_id}
                        required
                        value={this.state.data.store_id}
                        helperText = {dirty.store_id && !data.store_id ? 'Store is required' : ''}
                    >
                        {storeListLoading ? <MenuItem value=''>Fetching stores...</MenuItem> : 
                            storeList.map(e => <MenuItem key={e.id} value={e.id}>{e.name}</MenuItem>)}
                    </Select>
                </FormControl>
                <TextField 
                    style={styles.inputs} 
                    name='orderNo' 
                    label="Order Number" 
                    variant='outlined' 
                    error={dirty.orderNo && !data.orderNo}
                    required
                    helperText = {dirty.orderNo && !data.orderNo ? 'Order Number is required' : ''}
                />
                <TextField 
                    style={styles.inputs} 
                    name='orderAmt' 
                    label="Order Amount" 
                    type='number' 
                    variant="outlined" 
                    error={dirty.orderAmt && !data.orderAmt}
                    required
                    helperText = {dirty.orderAmt && !data.orderAmt ? 'Order Amount is required' : ''}
                />
                <Button 
                    style={styles.btn} 
                    color='primary' 
                    disabled={!data.orderNo || !data.orderAmt || !data.store_id || orderCreateLoading} 
                    variant='contained'
                    onClick={() => this.submitForm()}
                >{orderCreateLoading ? 'Creating...' : 'Create'}</Button>
            </form>
        )
    }
}

const styles = {
    form: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    inputs: {
        width: '100%',
        marginBottom: '20px'
    },
    btn:{
        width: '200px',
        textAlign: 'center',
        padding: '10px',
        marginBottom: '20px'
    }
}

const mapStateToProps = state => state;
export default connect(mapStateToProps)(CreateOrder);